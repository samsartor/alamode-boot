boot() {
    if [ -z $1 ]; then
        devices=$(find /dev \( -name "sd*[0-9]" -o -name "nvme*p[0-9]" \))
    else
        devices=$1
    fi

    mntpt=${3:-"/mnt"}

    for device in $devices; do
        mount $device $mntpt || continue

        if [ ! -d $mntpt/loader ]; then
            echo "$device is not structured for systemd-boot"
            umount $mntpt
            continue
        fi

        while read var value; do
            if [ $var == default ]; then
                default_entry=${2:-$value}
            fi
        done < $mntpt/loader/loader.conf

        if [ -z $default_entry ] || [ ! -f $mntpt/loader/entries/$default_entry.conf ]; then
            echo "No entry to boot"
            umount $mntpt
            continue
        fi

        while read var value; do
            case $var in
            linux)
                entry_linux="${mntpt}${value}"
                ;;
            initrd)
                flag_initrd=--initrd
                entry_initrd="${mntpt}${value}"
                ;;
            options)
                flag_options=--append
                entry_options="$value"
                ;;
            esac
        done < $mntpt/loader/entries/$default_entry.conf

        kexec -f $entry_linux $flag_initrd $entry_initrd $flag_options $entry_options
    done
}
