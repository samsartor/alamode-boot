#!/bin/bash
# Copyright 2020 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

VERSION="1.1.0"
SCRIPT=$(basename -- "${0}")
set -e

export LC_ALL=C

if [[ "$#" -lt 1 ]]; then
  echo "Usage: ${SCRIPT} variant_name"
  echo "e.g. ${SCRIPT} kohaku"
  echo "Copy the FIT image results from 'outputs' to 'files' and commit."
  echo "Script version ${VERSION}"
  exit 1
fi

# This is the name of the variant that has been created.
# ${var,,} converts to all lowercase.
VARIANT="${1,,}"

# All of the necessary files are in the parent directory from this script.
pushd "${BASH_SOURCE%/*}/.."

FITIMAGE="fitimage-${VARIANT}.bin"
VERSIONS="fitimage-${VARIANT}-versions.txt"
LOG="fit.log"

OUTPUT_DIR="asset_generation/outputs"
FILES_DIR="files"

# Make sure the files exist.
if [[ ! -e "${OUTPUT_DIR}/${FITIMAGE}" ]]; then
  echo "${FITIMAGE} not found."
  exit 1
fi

if [[ ! -e "${OUTPUT_DIR}/${VERSIONS}" ]]; then
  echo "${VERSIONS} not found."
  exit 1
fi

if [[ ! -e "${OUTPUT_DIR}/${LOG}" ]]; then
  echo "${LOG} not found."
  exit 1
fi

remove_added_file() {
  if [[ -e "$1" ]] ; then
    rm -f "$1"
    git restore --staged "$1" || true
  fi
}

cleanup() {
  # Clean up modified files and remove new ones
  git restore --staged "${OUTPUT_DIR}/${LOG}"
  git restore "${OUTPUT_DIR}/${LOG}"
  remove_added_file "${FILES_DIR}/${FITIMAGE}"
  remove_added_file "${FILES_DIR}/${VERSIONS}"
  # Find the name of the current branch and abandon it.
  BRANCH=$(git symbolic-ref -q HEAD | rev | cut -d/ -f1 | rev)
  repo abandon "${BRANCH}" .
}
trap 'cleanup' ERR

cp "${OUTPUT_DIR}/${FITIMAGE}" "${FILES_DIR}"
cp "${OUTPUT_DIR}/${VERSIONS}" "${FILES_DIR}"
git add "${OUTPUT_DIR}/${LOG}"
git add "${FILES_DIR}/${FITIMAGE}"
git add "${FILES_DIR}/${VERSIONS}"
git commit --amend --no-edit
