#!/bin/bash
# Copyright 2019 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Note that this script can be run inside or outside the chroot; only
# the `emerge` command listed in the commit message has to run inside
# the chroot.

VERSION="1.2.0"
SCRIPT=$(basename -- "${0}")
set -e

export LC_ALL=C

if [[ "$#" -lt 1 ]]; then
  echo "Usage: ${SCRIPT} variant_name [bug_number]"
  echo "e.g. ${SCRIPT} kohaku b:140261109"
  echo "Generate the FIT image for a variant of the Hatch base board"
  exit 1
fi

# shellcheck source=/mnt/host/source/src/platform/src/dev/contrib/variant/check_standalone.sh
# shellcheck disable=SC1091
source "${CROS_WORKON_SRCROOT}/src/platform/dev/contrib/variant/check_standalone.sh"

# Note that this script is specific to Hatch, and so it does not allow
# you to specify the baseboard as one of the cmdline arguments.
#
# This is the name of the base board that we're cloning to make the variant.
BASE="hatch"
# This is the name of the variant that is being cloned.
# ${var,,} converts to all lowercase.
VARIANT="${1,,}"

TEMPLATE_XSL="asset_generation/template_overrides.xsl"
VARIANT_XSL="asset_generation/${VARIANT}_overrides.xsl"

# Assign BUG= text, or "None" if that parameter wasn't specified.
BUG=${2:-None}

# All of the necessary files are in the parent directory from this script.
pushd "${BASH_SOURCE%/*}/.."

# Make sure the variant doesn't already exist.
if [[ -e "${VARIANT_XSL}" ]]; then
  echo "${VARIANT_XSL} already exists."
  echo "Have you already created this variant?"
  exit 1
fi

if [[ -e "files/fitimage-${VARIANT}.bin" ]]; then
  echo "files/fitimage-${VARIANT}.bin already exists."
  echo "Have you already created this variant?"
  exit 1
fi

# Make sure the template exists.
if [[ ! -e "${TEMPLATE_XSL}" ]]; then
  echo "${TEMPLATE_XSL} for the baseboard does not exist."
  exit 1
fi

# Start a branch. Use YMD timestamp to avoid collisions.
DATE=$(date +%Y%m%d)
BRANCH="create_${VARIANT}_${DATE}"
repo start "${BRANCH}" . "${NEW_VARIANT_WIP:+--head}"
# ${parameter:+word}" substitutes "word" if $parameter is set to a non-null
# value, or substitutes null if $parameter is null or unset.

cleanup() {
  # If there is an error after the `repo start`, then restore modified files
  # to clean up and `repo abandon` the new branch.
  if [[ -e "${VARIANT_XSL}" ]] ; then
    rm -f "${VARIANT_XSL}"
    # Use || true so that if the new files haven't been added yet, the error
    # won't terminate the script before we can finish cleaning up.
    git restore --staged "${VARIANT_XSL}" || true
  fi
  repo abandon "${BRANCH}" .
}
trap 'cleanup' ERR

sed -e "s/fitimage-BOARD/fitimage-${VARIANT}/" \
    "${TEMPLATE_XSL}" > "${VARIANT_XSL}"
git add "${VARIANT_XSL}"

# We can't run gen_fit_image.sh inside the chroot, because it uses the
# Intel TXE tools, which use wine to run on linux.
# There is a linux version, but it requires libGL, which is not in the chroot.
#
# The user must run gen_fit_image outside the chroot, git add the files,
# git commit --amend, and then upload the CL.

# Now commit the files.
git commit -m "${BASE}: Add fitimage for ${VARIANT}

Create the fitimage for the ${VARIANT} variant of the ${BASE} baseboard
by copying the template_overrides.xsl into a new XSL file for the variant.
Also add the generated fitimage, a manifest with version numbers and/or
SHA-256 hashes of critical files provided by FIT, and the log file from
generating the fitimage.

(Auto-Generated by ${SCRIPT} version ${VERSION}).

BUG=${BUG}
TEST=FW_NAME=${VARIANT} emerge-${BASE} coreboot-private-files-${BASE}
look for descriptor-${VARIANT}.bin in
/build/${BASE}/firmware/coreboot-private/3rdparty/blobs/baseboard/${BASE}"

# Note that the CL is not complete until the user runs gen_fit_image.sh
# outside the chroot, and then runs the commit_fitimage.sh to amend
# the commit with the generated files.

check_standalone "$(pwd)" "${BRANCH}"
