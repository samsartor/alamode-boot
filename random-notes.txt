Get into developer mode:
    Esc+F3+Power

    Ctrl+D on the recovery screen

Go to a different TTY:
    Ctrl+Alt+F2

    Type: root

Then ``gsctool -a -o`` and press the power button whenever it asks for it. This
enables CCD for one boot.

Plug in the SuzyQable. Try all orientations and all ports until ``lsusb`` shows
two new devices from ``Google Inc``.

    Bus 001 Device 045: ID 18d1:5014 Google Inc.
    Bus 001 Device 040: ID 18d1:501f Google Inc. SuzyQable

screen /dev/ttyUSB0

press enter until you get the ``>``

``help`` to see commands

``ccd reset factory`` allows you to access all CCD functionality even after
reboot.

Verify that write-protect is disabled "at boot" by typing ``wp``.

```
> wp
Flash WP: forced disabled
 at boot: forced disabled
```

BACKUP YOUR FIRMWARE

```
sudo ./flashrom -p raiden_debug_spi:target=AP -r ../backup_ap_firmware_sumner.bin
```

sudo -- run as root
./flashrom -- use the right one, the one that you compiled from AlamodeBoot
-p -- specifies that the next arg is the protocol to speak
raiden_debug_spi -- is the protocol that the SuzyQable speaks
target=AP -- the application processor (the Intel x86 chip's firmwared)
-r -- read the firmware (THIS IS IMPORTANT!)
../backup_ap_firmware_sumner.bin -- where to dump the firmware

Check your firmware backup:

```
make futility
bin/futility dump_fmap -H backup_ap_firmware_sumner.bin
```

Install the new firmware over the SuzyQable:

```
make install-raiden
```

NOTES:

* Linux needs the actual Linux executable, an initramfs, and the command line
  arguments.

  * Everything is specified in the menuconfig for coreboot.
  * Each has to be built individually, though.
    * Linux: by compiling Linux (duh)
    * CL arguments: just write them
    * initramfs: buildroot does this shit

        * Buildroot takes whatever we configure in the menuconfig
            * This includes adding everything in $ALAMODE_BOOT/root to the
              initramfs
            * Busybox (libc, bin, etc, etc.)

* All the firmware does is run `/init` in the initramfs

Serial buses that you can console in to:

* CR50, G-chip, H1-chip
* AP: Application processor
* The EC is the embedded controller
