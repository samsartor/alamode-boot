# AlamodeBoot

The result of a bad idea of some former members of Mines LUG.

AlamodeBoot allows you to use Linux as your bootloader.

To brick your [kohaku](https://www.samsung.com/us/computing/chromebooks/12-14/galaxy-chromebook--256gb-storage--8gb-ram---fiesta-red-xe930qca-k01us/) type `make install-ap` (requires a SuzyQable and closed-case-debugging to be enabled).

NOTES:

```
rustup target install x86_64-unknown-linux-musl
```
