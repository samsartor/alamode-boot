.PHONY: all utils clean install-raiden simulate

all: bin/flashrom boot.rom
utils: bin/futility bin/cbfsutil


# Download the Linux kernel sources
linux-5.4.74.tar.xz:
	wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.4.74.tar.xz

# Unzip the Linux kernel
linux: linux-5.4.74.tar.xz
	mkdir -p linux
	tar xf linux-5.4.74.tar.xz -C linux --strip-components=1

# Build a Linux binary using linux.config
linux/arch/x86/boot/bzImage: linux.config | linux
	cp linux.config linux/.config
	+cd linux && $(MAKE)

clean:
	rm -rf bin
	rm -rf root/bin/alamode
	+cd linux && $(MAKE) clean
	+cd flashrom && $(MAKE) clean
	+cd coreboot && $(MAKE) clean
	+cd buildroot && $(MAKE) clean

# Create a fake USB Image
fakeusb.iso: fakeusb.sh linux/arch/x86/boot/bzImage
	./fakeusb.sh fakeusb.iso

# Build a utility to flash the Chromebook's firmware.
bin/flashrom:
	mkdir -p bin
	+cd flashrom && CC=clang $(MAKE) flashrom CONFIG_RAIDEN_DEBUG_SPI=yes CONFIG_INTERNAL=yes CONFIG_DUMMY=yes CONFIG_DEFAULT_PROGRAMMER=PROGRAMMER_INTERNAL
	cp flashrom/flashrom bin

# Enable only the futility commands we need to save space
FUTIL_CMDS ?= dump_fmap dump_kernel_config vbutil_kernel
FUTIL_BASE_FILES ?= \
	futility/file_type.c \
	futility/file_type_bios.c \
	futility/file_type_rwsig.c \
	futility/futility.c \
	futility/misc.c \
	futility/vb1_helper.c \
	futility/vb2_helper.c

# Build a utility to look at the firmware image
bin/futility:
	mkdir -p bin
	+cd coreboot/3rdparty/vboot && $(MAKE) futil \
		CC="gcc -flto -Wl,--unresolved-symbols=ignore-in-object-files" \
		STATIC=y \
		FUTIL_SRCS="$(foreach cmd,$(FUTIL_CMDS),futility/cmd_$(cmd).c) $(FUTIL_BASE_FILES)" \
		FUTIL_LIBS=""
	cp coreboot/3rdparty/vboot/build/futility/futility bin

# Build another utility to look at the firmware image
bin/cbfsutil:
	+cd coreboot && $(MAKE) build/cbfstool
	cp coreboot/build/cbfstool bin

alamode_bin:
	mkdir -p root/bin/alamode

ncurses-6.2.tar.gz:
	wget https://ftp.gnu.org/pub/gnu/ncurses/ncurses-6.2.tar.gz

ncurses-6.2/configure:
	tar xzf ncurses-6.2.tar.gz

lib/libncurses.a: ncurses-6.2/configure
	cd ncurses-6.2 && ./configure --prefix=$(PWD) CC=musl-gcc
	cd ncurses-6.2 && make install

# This program allows you to cleanly shutdown the firmware's Linux.
root/bin/alamode/halt: src/halt.c | alamode_bin
	musl-gcc -static -Os $< -o $@

# A rust TUI for selecting a boot device
root/bin/alamode/bootdev: $(shell find bootdev-rs/src) | alamode_bin
	# TODO: don't use musl
	cd bootdev-rs && cargo build --release --target x86_64-unknown-linux-musl
	cp bootdev-rs/target/x86_64-unknown-linux-musl/release/bootdev $@
	strip $@

root/bin/alamode/futility: bin/futility
	cp $< $@
	strip $@

# Create the rootfs
buildroot/output/images/rootfs.cpio.xz: buildroot.config busybox.config root/bin/alamode/halt root/bin/alamode/bootdev $(shell find root)
ifeq (,$(wildcard /usr/bin/file))
	# Not really correct on nixos but we don't have a choice AFAIK
	echo "Please place $(shell which file) into /usr/bin"
	false
endif
	cp buildroot.config buildroot/.config
	+cd buildroot && $(MAKE)

# Add a tool to cross-compile GCC
coreboot/util/crossgcc/xgcc/bin/i386-elf-gcc:
	+cd coreboot && $(MAKE) crossgcc-i386 CPUS=$(nproc)

# Build the boot image using coreboot
boot.rom: coreboot.config buildroot/output/images/rootfs.cpio.xz linux/arch/x86/boot/bzImage coreboot/util/crossgcc/xgcc/bin/i386-elf-gcc
	cp coreboot.config coreboot/.config
	+cd coreboot && $(MAKE)
	cp coreboot/build/coreboot.rom $@

# Flash it to a Kohaku plugged in via SuzyQable (or equivalant)
install-raiden: boot.rom bin/flashrom
	sudo bin/flashrom --noverify -p raiden_debug_spi:target=AP -w boot.rom

# Flash it to this Kohaku
install-host: boot.rom bin/flashrom
	sudo bin/flashrom --noverify -p host -w boot.rom

simulate: buildroot/output/images/rootfs.cpio.xz linux/arch/x86/boot/bzImage fakeusb.iso
	qemu-system-x86_64 -enable-kvm -kernel linux/arch/x86/boot/bzImage \
		-initrd buildroot/output/images/rootfs.cpio.xz \
		-serial stdio \
		-drive id=fakeusb,file=fakeusb.iso,format=raw,if=none \
		-device qemu-xhci,id=usb-c \
		-device usb-storage,drive=fakeusb,bus=usb-c.0
