use std::{convert::Infallible, ffi::CString, fmt::Debug, os::unix::io::IntoRawFd};
use anyhow::{Error, bail};

pub fn prepare_kexec(
    linux: impl IntoRawFd + Debug,
    initrd: Option<impl IntoRawFd + Debug>,
    options: &str,
) -> Result<(), Error> {
    use libc::{syscall, SYS_kexec_file_load, c_long};

    dbg!(&linux, &initrd, options);

    let options = CString::new(options).unwrap();
    let linux = linux.into_raw_fd() as c_long;
    let (flags, initrd) = match initrd {
        Some(f) => (0x0, f.into_raw_fd() as c_long),
        None => (0x4 /* KEXEC_FILE_NO_INITRAMFS */, 0),
    };

    let result = unsafe {
        syscall(
            SYS_kexec_file_load,
            linux,
            initrd,
            options.as_bytes_with_nul().len() as c_long,
            options.as_ptr() as c_long,
            flags,
        )
    };

    if result != 0 {
        bail!("could not load kernel to kexec");
    }

    Ok(())
}

pub fn reboot_kexec() -> Result<Infallible, Error> {
    use nix::sys::reboot;

    reboot::reboot(reboot::RebootMode::RB_KEXEC).map_err(Error::from)
}
