use anyhow::{anyhow, bail, Error};
use std::{
    fs::{read_dir, File},
    io::{BufRead, BufReader},
    path::{Path, PathBuf},
};

struct Parts {
    parts: Vec<String>,
    current: String,
    quoted: bool,
    escaped: bool,
}

impl Parts {
    fn push(&mut self, c: char) {
        if self.escaped {
            self.current.push(c);
            self.escaped = false;
        } else if c.is_whitespace() {
            if self.quoted {
                self.current.push(c);
            } else if !self.current.is_empty() {
                self.parts.push(self.current.split_off(0));
            }
        } else {
            if c == '\'' {
                if self.current.is_empty() && !self.quoted {
                    self.quoted = true;
                } else if self.quoted {
                    self.parts.push(self.current.split_off(0));
                    self.quoted = false;
                } else {
                    self.current.push(c);
                }
            } else if c == '\\' {
                self.escaped = true;
            } else {
                self.current.push(c);
            }
        }
    }
}

fn read_line(line: &str) -> Vec<String> {
    let mut r = Parts { parts: Vec::new(), current: String::new(), quoted: false, escaped: false };

    for c in line.chars() {
        r.push(c);
    }

    if !r.current.is_empty() {
        r.parts.push(r.current);
    }

    r.parts
}

fn read_file(filepath: &PathBuf, mountpoint: &Path) -> Result<Vec<super::Entry>, Error> {
    let mut entries = Vec::new();
    let file = BufReader::new(File::open(filepath)?);
    let mut entry = None;

    for line in file.lines() {
        let line = read_line(&line?);
        let command = match line.get(0) {
            Some(c) => {
                if c.starts_with("#") {
                    continue
                }
                c
            },
            None => continue,
        };
        dbg!(&line);

        match (command.as_str(), &mut entry) {
            ("source", None) => {
                // TODO: make this way more robust. Right now, I'm just assuming
                // that the source command is not in the middle of an existing
                // entry, or anything strange like that.
                if let Some(source_ref) = line.get(1) {
                    let sourced_mountpoint =
                        mountpoint.join(source_ref.strip_prefix("/").unwrap_or(source_ref));
                    if let Ok(mut sourced_entries) = read_file(&sourced_mountpoint, mountpoint) {
                        entries.append(&mut sourced_entries);
                    }
                }
            },
            ("menuentry", None) => {
                entry = Some(super::Entry {
                    name: line.get(1).ok_or(anyhow!("missing title"))?.clone().into(),
                    title: None,
                    linux: None,
                    description: None,
                    initrd: None,
                    options: None,
                    time: None,
                });
            },
            ("linux", Some(e)) => {
                e.linux = line.get(1).cloned();
                if line.len() > 2 {
                    e.options = Some(
                        line.iter()
                            // Ignore the first two elements of the list (we
                            // already pulled them out).
                            .skip(2)
                            // Just ignore all variables. It's a hack, but it
                            // at least works for NixOS installers.
                            .filter(|x| !x.starts_with("$"))
                            .map(|s| &**s)
                            .collect::<Vec<&str>>()
                            .join(" "),
                    );
                }
            },
            ("initrd", Some(e)) if line.len() > 1 => {
                // TODO: microcode
                e.initrd = Some(line.last().unwrap().clone());
            },
            ("}", Some(_)) => {
                entries.push(entry.take().unwrap());
            },
            _ => (),
        }
    }
    Ok(entries)
}

pub fn read(boot: &mut super::Boot) -> Result<(), Error> {
    let mut root = boot.mount.path().to_owned();
    let mut grub_dir = root.join("grub");
    if !grub_dir.exists() {
        root = root.join("boot");
        grub_dir = root.join("grub");
    }
    if !grub_dir.exists() {
        bail!("not configured for GRUB");
    }

    for file in read_dir(grub_dir)? {
        let file = file?;
        let path = file.path();
        dbg!("Trying to find GRUB config files in:", &path);
        if path.extension().map(|e| e != "conf" && e != "cfg").unwrap_or(true) {
            continue
        }
        dbg!("Read GRUB config at", &path);

        // Have to pass in the mount path, just in case we need to source (that's
        // relative to the mount path).
        if let Ok(mut entries) = read_file(&path, boot.mount.path()) {
            boot.entries.append(&mut entries);
        }
    }

    Ok(())
}
