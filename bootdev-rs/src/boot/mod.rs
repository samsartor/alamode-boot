use std::{
    convert::Infallible,
    ffi::OsStr,
    ffi::OsString,
    fs::File,
    path::{Path, PathBuf},
    time::SystemTime,
};

mod grub;
mod systemd;

use crate::mount::Mount;

use anyhow::{bail, Error};

pub struct Boot {
    pub mount: Mount,
    pub default: Option<OsString>,
    pub entries: Vec<Entry>,
    pub selected: tui::widgets::ListState,
}

impl Boot {
    pub fn new(device: OsString) -> Result<Self, BootError> {
        let mount = Mount::new(Path::new("/dev").join(&device))
            .map_err(|error| BootError { error, device })?;
        let mut boot = Boot {
            mount,
            default: None,
            entries: Vec::new(),
            selected: Default::default(),
        };
        let _ = systemd::read(&mut boot);
        let _ = grub::read(&mut boot);
        boot.entries
            .sort_by(|a, b| std::cmp::Ord::cmp(&b.time, &a.time));
        Ok(boot)
    }

    pub fn device(&self) -> &OsStr {
        self.mount.source().file_name().unwrap()
    }

    fn prepare_path(&self, path: &Path) -> PathBuf {
        match path.strip_prefix("/") {
            Ok(p) => self.mount.path().join(p),
            Err(_) => self.mount.path().join(path),
        }
    }

    pub fn kexec(&self, index: usize) -> Result<Infallible, Error> {
        use crate::kexec::{prepare_kexec, reboot_kexec};
        let ent = &self.entries[index];

        prepare_kexec(
            match &ent.linux {
                None => bail!("kernel image not specified"),
                Some(l) => File::open(self.prepare_path(Path::new(l)))?,
            },
            match &ent.initrd {
                None => None,
                Some(l) => Some(File::open(self.prepare_path(Path::new(l)))?),
            },
            ent.options.as_ref().map(String::as_str).unwrap_or(""),
        )?;
        reboot_kexec()
    }

    pub fn kexec_selected(&self) -> Result<(), Error> {
        if let Some(i) = self.selected.selected() {
            self.kexec(i)?;
        }

        Ok(())
    }
}

pub struct Entry {
    pub name: OsString,
    pub title: Option<String>,
    pub description: Option<String>,
    pub linux: Option<String>,
    pub initrd: Option<String>,
    pub options: Option<String>,
    pub time: Option<SystemTime>,
}

pub struct BootError {
    pub error: Error,
    pub device: OsString,
}

pub type BootResult = Result<Boot, BootError>;

pub async fn scan(device: OsString) -> BootResult {
    use smol::{lock::Semaphore, unblock};

    static SCAN_LIMIT: Semaphore = Semaphore::new(4);

    let _scanning = SCAN_LIMIT.acquire().await;
    unblock(|| Boot::new(device)).await
}
