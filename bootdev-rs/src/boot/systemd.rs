use std::{
    ffi::{OsStr, OsString},
    fs::{metadata, read_dir, File},
    io::{BufRead, BufReader},
    path::Path,
};

use anyhow::{bail, Error};

fn read_conf(path: &Path, mut entries: impl FnMut(&str, &str)) -> Result<(), Error> {
    dbg!("Read systemd-boot config at", &path);
    for line in BufReader::new(File::open(path)?).lines() {
        let line = line?;
        let line = line.trim_start();
        if let Some(div) = line.find(char::is_whitespace) {
            let (key, value) = line.split_at(div);
            entries(key, value.trim());
        }
    }

    Ok(())
}

pub fn read(boot: &mut super::Boot) -> Result<(), Error> {
    const CONF_LOCATION: &str = "loader/loader.conf";

    let mut root = boot.mount.path().to_owned();
    let mut config_path = root.join(CONF_LOCATION);
    if !config_path.exists() {
        root = root.join("boot");
        config_path = root.join(CONF_LOCATION);
    }
    if !config_path.exists() {
        bail!("not configured for systemd");
    }

    read_conf(&config_path, |key, value| match key {
        "default" => boot.default = Path::new(value).file_stem().map(OsStr::to_owned),
        _ => (),
    })?;

    for file in read_dir(&config_path.parent().unwrap().join("entries"))? {
        let file = file?;
        let path = file.path();
        if path.extension().map(|e| e != "conf").unwrap_or(true) {
            continue
        }

        let name = path.file_stem().map(OsStr::to_owned).unwrap_or(OsString::new());
        let meta = metadata(&path)?;
        let mut entry = super::Entry {
            name,
            title: None,
            description: None,
            linux: None,
            initrd: None,
            options: None,
            time: meta.accessed().or(meta.modified()).or(meta.created()).ok(),
        };

        read_conf(&path, |key, value| match key {
            "title" => entry.title = Some(value.to_string()),
            "version" => entry.description = Some(value.to_string()),
            "linux" => entry.linux = Some(value.to_string()),
            "initrd" => entry.initrd = Some(value.to_string()),
            "options" => entry.options = Some(value.to_string()),
            _ => (),
        })?;

        boot.entries.push(entry);
    }

    Ok(())
}
