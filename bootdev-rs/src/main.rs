use crate::boot::BootResult;
use crate::partitions::Disk;
use crate::update::Update;
use anyhow::Error;
use std::{
    borrow::Cow,
    collections::BTreeMap,
    ffi::{OsStr, OsString},
    fmt::Write,
    io,
};
use termion::{event::Event, raw::IntoRawMode};
use tui::{
    backend::TermionBackend,
    layout::{Alignment, Constraint, Direction, Layout, Rect},
    style::{Color, Modifier, Style},
    widgets as tw, Terminal,
};

mod boot;
mod kexec;
mod mount;
mod partitions;
mod update;

type Frame<'a, 'b> =
    &'a mut tui::terminal::Frame<'b, TermionBackend<termion::raw::RawTerminal<io::Stdout>>>;

/// These are the root menu options
#[derive(Copy, Clone, Eq, PartialEq)]
enum Action {
    Auto,
    Boot,
    Shell,
}

impl Action {
    const ALL: &'static [Action] = &[Action::Auto, Action::Boot, Action::Shell];

    fn tui_item(self, countdown: Option<usize>) -> tw::ListItem<'static> {
        use Action::*;

        match self {
            Auto => {
                if let Some(i) = countdown {
                    tw::ListItem::new(format!("Automatic ({})", i))
                } else {
                    tw::ListItem::new("Automatic")
                }
            }
            Boot => tw::ListItem::new("Choose Boot Device"),
            Shell => tw::ListItem::new("Shell"),
        }
    }

    fn tui_list(at_level: bool, countdown: Option<usize>) -> tw::List<'static> {
        tui_list_from_items(
            "Actions".to_string(),
            Action::ALL
                .iter()
                .copied()
                .map(|i| Action::tui_item(i, countdown))
                .collect::<Vec<_>>(),
            at_level,
        )
    }
}

fn tui_center_text(text: String) -> tw::Paragraph<'static> {
    tw::Paragraph::new(text)
        .alignment(Alignment::Center)
        .block(tw::Block::default().borders(tw::Borders::ALL))
        .style(Style::default().fg(Color::White))
}

fn tui_list_from_items(
    name: String,
    items: Vec<tw::ListItem<'static>>,
    at_level: bool,
) -> tw::List<'static> {
    let hstyle = if at_level {
        Style::default().bg(Color::White).fg(Color::Black)
    } else {
        Style::default()
    };
    tw::List::new(items)
        .style(Style::default().fg(Color::White))
        .block(tw::Block::default().borders(tw::Borders::ALL).title(name))
        .highlight_style(hstyle.add_modifier(Modifier::BOLD))
        .highlight_symbol(">> ")
}

/// Select a different element of the list with wraparound.
fn move_select(state: &mut tw::ListState, by: isize, len: usize) {
    if len == 0 {
        state.select(None);
        return;
    }

    let leni = len as isize;
    let by = by % leni;

    if let Some(mut i) = state.selected() {
        i += if by < 0 {
            (leni + by) as usize
        } else {
            by as usize
        };
        state.select(Some(i % len));
    } else {
        state.select(Some(0));
    }
}

#[derive(Default)]
struct App {
    level: usize,
    countdown: Option<usize>,
    action: tw::ListState,
    disk: tw::ListState,
    disks: Vec<Disk>,
    options: BTreeMap<OsString, BootResult>,
    running: bool,
}

impl App {
    fn new() -> App {
        let mut app = App::default();
        app.countdown = Some(5);
        app.running = true;
        app.action.select(Some(0));
        app
    }

    fn move_row(&mut self, by: isize) {
        match self.level {
            0 => move_select(&mut self.action, by, Action::ALL.len()),
            1 => move_select(
                &mut self.disk,
                by,
                self.disks.iter().map(|d| d.partitions.len()).sum(),
            ),
            2 => {
                if let Some(Ok(b)) = self.get_boot_mut() {
                    move_select(&mut b.selected, by, b.entries.len());
                }
            }
            _ => (),
        }
        self.react_selection();
    }

    fn move_column(&mut self, by: isize) {
        let last_column = match self.action() {
            Action::Boot => 2,
            _ => 0,
        };
        self.level = (self.level as isize + by).min(last_column).max(0) as usize;
        self.move_row(0);
    }

    fn react_selection(&mut self) {
        self.countdown = None;
    }

    fn action(&self) -> Action {
        let i = self.action.selected().unwrap_or(0);
        Action::ALL[i]
    }

    fn partitions(&self) -> impl Iterator<Item = &OsStr> {
        self.disks
            .iter()
            .flat_map(|d| &d.partitions)
            .map(|p| p.1.as_os_str())
    }

    fn set_part(&mut self, name: &OsStr) {
        let pos = self.partitions().position(|n| n == name);
        if let Some(i) = pos {
            self.disk.select(Some(i));
        } else {
            self.disk.select(None);
        }
    }

    fn get_part(&self) -> Option<&OsStr> {
        let i = self.disk.selected()?;
        self.partitions().nth(i)
    }

    fn get_boot_mut(&mut self) -> Option<&mut BootResult> {
        // TODO: don't allocate. Just separate the borrow for get_part and options.
        self.options.get_mut(&self.get_part()?.to_owned())
    }

    fn tick(&mut self) -> Result<(), Error> {
        match self.countdown {
            Some(0) => {
                self.running = false;
            }
            Some(i) => {
                self.countdown = Some(i - 1);
            }
            None => (),
        }

        Ok(())
    }

    /// Handles inputs. This allows you to use the normal up/down/left/right
    /// keys as well as Vim bindings (HJKL). This also supports the 3l
    /// version of Vim bindings which are located at QWERTY SVUM.
    fn input(&mut self, event: Event) -> Result<(), Error> {
        use termion::event::{Event::*, Key::*};

        match event {
            Key(Char('q')) => {
                self.running = false;
            }
            Key(Char('\n')) => {
                if let Action::Shell | Action::Auto = self.action() {
                    self.running = false;
                } else if Action::Boot == self.action() && self.level == 2 {
                    if let Some(Ok(b)) = self.get_boot_mut() {
                        if let Err(e) = b.kexec_selected() {
                            eprintln!("Could not kexec into {:?}: {}", b.device(), e);
                        }
                    }
                } else {
                    self.move_column(1);
                }
            }
            Key(Right) | Key(Char('l')) | Key(Char('L')) | Key(Char('m')) | Key(Char('M')) => {
                self.move_column(1);
            }
            Key(Left) | Key(Char('h')) | Key(Char('H')) | Key(Char('s')) | Key(Char('S')) => {
                self.move_column(-1);
            }
            Key(Down) | Key(Char('j')) | Key(Char('J')) | Key(Char('v')) | Key(Char('V')) => {
                self.move_row(1);
            }
            Key(Up) | Key(Char('k')) | Key(Char('K')) | Key(Char('u')) | Key(Char('U')) => {
                self.move_row(-1);
            }
            _ => (),
        }

        Ok(())
    }

    fn view_disks(&mut self, f: Frame, r: Rect) {
        let mut disks = Vec::new();
        for d in &self.disks {
            d.tui_items(&mut disks);
        }
        f.render_stateful_widget(
            tui_list_from_items("Partitions".to_string(), disks, self.level == 1),
            r,
            &mut self.disk,
        );
    }

    fn view_bopts(&mut self, f: Frame, r: Rect) {
        let at_level = self.level == 2;
        let has_part = self.get_part().is_some();
        match self.get_boot_mut() {
            Some(Ok(b)) if b.entries.is_empty() => {
                f.render_widget(tui_center_text("No boot config found".to_string()), r)
            }
            Some(Ok(b)) => {
                f.render_stateful_widget(
                    tui_list_from_items(
                        b.mount.source().display().to_string(),
                        b.entries
                            .iter()
                            .map(|e| {
                                let mut title = match &e.title {
                                    Some(t) => Cow::from(t.as_str()),
                                    _ => e.name.to_string_lossy(),
                                };
                                if let Some(desc) = &e.description {
                                    write!(title.to_mut(), " ({})", desc).unwrap()
                                }
                                tw::ListItem::new(title.into_owned())
                            })
                            .collect(),
                        at_level,
                    ),
                    r,
                    &mut b.selected,
                );
            }
            Some(Err(e)) => f.render_widget(tui_center_text(e.error.to_string()), r),
            None if has_part => f.render_widget(tui_center_text("Loading...".to_string()), r),
            None => (),
        }
    }

    /// Renders the entire UI
    fn view(&mut self, f: Frame) {
        let mut columns = [
            Constraint::Percentage(25),
            Constraint::Percentage(25),
            Constraint::Percentage(25),
        ];

        if let Some(c) = columns.get_mut(self.level) {
            *c = Constraint::Percentage(50);
        }

        let rects = Layout::default()
            .direction(Direction::Horizontal)
            .constraints(columns.as_ref())
            .split(f.size());

        // Render the actions
        f.render_stateful_widget(
            Action::tui_list(self.level == 0, self.countdown),
            rects[0],
            &mut self.action,
        );

        // Render the disk partitions
        if self.action() == Action::Boot {
            self.view_disks(f, rects[1]);
            self.view_bopts(f, rects[2]);
        }
    }

    async fn finalize(self) -> Result<(), Error> {
        match self.action() {
            Action::Auto => {
                for (_, boot) in self.options {
                    if let Ok(boot) = boot {
                        if boot.default.is_none() {
                            continue;
                        }
                        let default = boot.default.clone().unwrap();
                        if let Some(default_idx) =
                            boot.entries.iter().position(|x| x.name == default)
                        {
                            if let Err(e) = boot.kexec(default_idx) {
                                eprintln!(
                                    "Could not kexec into {:?} ({:?}): {}",
                                    boot.device(),
                                    boot.entries[default_idx].name,
                                    e
                                );
                            }
                        }
                    }
                }
            }
            _ => (),
        }

        Ok(())
    }
}

fn main() -> Result<(), Error> {
    // Terminal initialization
    let stdout = io::stdout().into_raw_mode()?;
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    terminal.clear()?;

    // App
    let mut app = App::new();
    app.tick()?;
    terminal.draw(|f| app.view(f))?;

    update::run(|u| {
        match u {
            Update::End => app.running = false,
            Update::Input(e) => {
                if let Err(e) = app.input(e) {
                    eprintln!("{:?}", e);
                }
            }
            Update::Tick => {
                if let Err(e) = app.tick() {
                    eprintln!("{:?}", e);
                }
            }
            Update::AddDisk(d) => {
                let selected = app.get_part().map(OsStr::to_owned);
                app.disks.push(d);
                app.disks.sort_by(|a, b| a.name.cmp(&b.name));
                if let Some(name) = selected {
                    app.set_part(&name);
                };
            }
            Update::RemoveDisk(n) => {
                let selected = app.get_part().map(OsStr::to_owned);
                app.disks.retain(|d| d.name != n);
                if let Some(name) = selected {
                    app.set_part(&name);
                };
            }
            Update::SetBoot(b) => {
                app.options.insert(
                    match &b {
                        Ok(b) => b.device().to_owned(),
                        Err(e) => e.device.clone(),
                    },
                    b,
                );
            }
        }
        if app.running {
            if let Err(e) = terminal.draw(|f| app.view(f)) {
                eprintln!("Could not draw app: {}", e);
                return false;
            }
        }
        app.running
    });

    terminal.clear()?;
    smol::block_on(app.finalize())
}
