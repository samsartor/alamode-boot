use std::{ffi::OsString, time::Duration};

use crate::{
    boot::{self, BootResult},
    partitions::{Disk, DiskScan},
};
use anyhow::Error;
use async_channel::{bounded, Sender};
use smol::{block_on, unblock, Executor, Timer};
use std::future::Future;
use termion::{event::Event, input::TermRead};

pub enum Update {
    End,
    Input(Event),
    AddDisk(Disk),
    RemoveDisk(OsString),
    SetBoot(BootResult),
    Tick,
}

pub static EXECUTOR: Executor = Executor::new();

pub async fn run_with<R>(
    inner: impl Future<Output = R>,
    chan: &Sender<Update>,
    tick_interval: Duration,
    scan_interval: Duration,
) -> R {
    let ticker_chan = chan.clone();
    let _ticker = EXECUTOR.spawn(async move {
        let mut tick = Timer::after(tick_interval);
        loop {
            (&mut tick).await;
            tick.set_after(tick_interval);
            let _ = ticker_chan.send(Update::Tick).await;
        }
    });

    let scanner_chan = chan.clone();
    let _scanner = EXECUTOR.spawn(async move {
        let mut scan = DiskScan::new();
        let mut tick = Timer::after(scan_interval);
        loop {
            (&mut tick).await;
            tick.set_after(scan_interval);

            let chan = scanner_chan.clone();
            scan = unblock(move || {
                scan.update(
                    |disk| {
                        for (_, device) in &disk.partitions {
                            let device = device.clone();
                            let chan = chan.clone();
                            EXECUTOR
                                .spawn(async move {
                                    let _ =
                                        chan.send(Update::SetBoot(boot::scan(device).await)).await;
                                })
                                .detach();
                        }
                        let _ = block_on(chan.send(Update::AddDisk(disk)));
                    },
                    |name| {
                        let _ = block_on(chan.send(Update::RemoveDisk(name)));
                    },
                );
                scan
            })
            .await;
        }
    });

    let input_chan = chan.clone();
    std::thread::spawn(move || {
        for event in std::io::stdin().events() {
            block_on(input_chan.send(Update::Input(event?)))?;
        }

        block_on(input_chan.send(Update::End))?;

        Result::<_, Error>::Ok(())
    });

    inner.await
}

pub fn run(mut update: impl FnMut(Update) -> bool) {
    let (s, r) = bounded(10);
    block_on(EXECUTOR.run(run_with(
        async move {
            loop {
                let up = r.recv().await;
                if let Ok(up) = up {
                    if !update(up) {
                        break;
                    }
                } else {
                    break;
                }
            }
        },
        &s,
        Duration::from_millis(1000),
        Duration::from_millis(500),
    )));
}
