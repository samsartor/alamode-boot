use anyhow::Error;
use std::fs::{read_dir, read_to_string, DirEntry};
use std::{collections::BTreeSet, ffi::OsString};
use tui::widgets as tw;

pub struct Disk {
    pub name: OsString,
    pub model: String,
    pub partitions: Vec<(usize, OsString)>,
}

impl Disk {
    pub fn tui_items(&self, items: &mut Vec<tw::ListItem<'static>>) {
        items.extend(self.partitions.iter().map(|(_, name)| {
            tw::ListItem::new(format!("{} ({})", name.to_string_lossy(), self.model))
        }));
    }
}

fn dir2disk(d: DirEntry) -> Result<Option<Disk>, Error> {
    let mut has_device = false;
    let mut model = "Unknown".to_string();
    let mut partitions = Vec::new();

    for e in read_dir(d.path())? {
        let e = e?;
        let name = e.file_name();
        if name == "device" {
            has_device = true;
            if let Ok(text) = read_to_string(e.path().join("model")) {
                model.replace_range(.., text.trim());
            }
        } else if e.file_type()?.is_dir() {
            if let Ok(index) = read_to_string(e.path().join("partition")) {
                partitions.push((index.trim().parse()?, name));
            }
        }
    }

    if !has_device {
        return Ok(None);
    }

    partitions.sort_by_key(|&(i, _)| i);

    Ok(Some(Disk {
        name: d.file_name(),
        model,
        partitions,
    }))
}

const BLOCKSYS: &str = "/sys/block/";

pub struct DiskScan {
    current: BTreeSet<OsString>,
}

impl DiskScan {
    pub fn new() -> Self {
        DiskScan {
            current: BTreeSet::new(),
        }
    }

    pub fn update(&mut self, mut on_add: impl FnMut(Disk), mut on_remove: impl FnMut(OsString)) {
        let mut prev = std::mem::replace(&mut self.current, BTreeSet::new());
        self.current.clear();

        let sys_dir = match read_dir(BLOCKSYS) {
            Ok(d) => d,
            Err(e) => {
                eprintln!("Could not read sysfs: {}", e);
                return;
            }
        };

        for block in sys_dir {
            let block = match block {
                Ok(b) => b,
                Err(e) => {
                    eprintln!("Could not read entry in sysfs: {}", e);
                    continue;
                }
            };

            let name = block.file_name();
            match prev.remove(&name) {
                true => {
                    self.current.insert(name);
                }
                false => match dir2disk(block) {
                    Ok(Some(d)) => {
                        on_add(d);
                        self.current.insert(name);
                    }
                    Ok(None) => (),
                    Err(e) => eprintln!("Bad data in sysfs for {:?}: {}", name, e),
                },
            }
        }

        for removed in prev {
            on_remove(removed);
        }
    }
}
