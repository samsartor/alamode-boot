use anyhow::{bail, Error};
use nix::mount::{mount, umount, MsFlags};
use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::{Path, PathBuf},
};
use tempdir::TempDir;
pub struct Mount {
    source: PathBuf,
    point: TempDir,
}

pub fn filesystems() -> Result<Vec<String>, Error> {
    let mut fss = Vec::new();
    for line in BufReader::new(File::open("/proc/filesystems")?).lines() {
        let line = line?;
        let mut parts = line.split_whitespace();
        let fs = match parts.next_back() {
            Some(p) => p,
            None => continue,
        };
        if parts.next() == Some("nodev") {
            continue
        }
        fss.push(fs.to_string());
    }
    Ok(fss)
}

impl Mount {
    pub fn new(source: PathBuf) -> Result<Self, Error> {
        let point = TempDir::new("/tmp/")?;
        let mut mounted = false;
        for fs in filesystems()? {
            if let Ok(_) = mount(
                Some(source.as_path()),
                point.path(),
                Some(fs.as_str()),
                MsFlags::MS_RDONLY,
                Option::<&Path>::None,
            ) {
                mounted = true;
                break
            }
        }
        if !mounted {
            bail!("could not find filesystem for {}", source.display());
        }
        Ok(Mount { source: source.to_owned(), point })
    }

    pub fn source(&self) -> &Path {
        &self.source
    }

    pub fn path(&self) -> &Path {
        self.point.path()
    }
}

impl Drop for Mount {
    fn drop(&mut self) {
        let _ = umount(self.point.path());
    }
}
