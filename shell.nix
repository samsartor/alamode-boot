let
  # Download the Mozilla nixpkgs overlay for getting Rust nightly.
  moz_overlay = import (builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz);
  pkgs = import <nixpkgs> {
    overlays = [
      moz_overlay
    ];
  };

  rust-nightly = (
    pkgs.latest.rustChannels.nightly.rust.override {
      # Need musl for static compilation.
      targets = [ "x86_64-unknown-linux-musl" ];
      extensions = [
        "rust-src"
        "rls-preview"
        "rust-analysis"
        "rustfmt-preview"
      ];
    }
  );
in
  with pkgs; mkShell {
    name = "alamode-boot";

    mesonFlags = lib.optional stdenv.isAarch64 [ "-Dpciutils=false" ];
    hardeningDisable = [ "all" ];
    PKG_CONFIG_ALLOW_CROSS = true;
    PKG_CONFIG_ALL_STATIC = true;
    LIBZ_SYS_STATIC = 1;

    nativeBuildInputs = [
      meson
      pkgconfig
      ninja
      file
    ];

    buildInputs = [
      bc
      bison
      clang
      cpio
      exfat
      flex
      libelf
      libusb1
      libuuid
      # uncomment when you need to compile the halt program
      # musl 
      ncurses
      openssl
      parted
      pciutils
      perl
      python2
      qemu
      rnix-lsp
      rsync
      unzip
      wget
      which
      zlib

      # Rust
      cargo
      rust-nightly
    ];
  }
