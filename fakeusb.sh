#!/usr/bin/env sh

set -ex

iso=$1
mnt=$(mktemp -d)
mkdir -p $mnt

echo "WARNING: I need sudo to do some loop device mounting stupidity. I'll do my best not to overwrite your drive. (press enter to continue)"
read

format_part() {
    part=$(sudo losetup --offset $(($2*512)) $iso --find --show)
    sudo mkfs.exfat $part
    sudo mount $part $mnt
    echo "If you are seeing this, you have mounted $1a fake USB drive." | sudo tee $mnt/README.txt
    if [ -n "$3" ]; then
        sudo cp linux/arch/x86/boot/bzImage $mnt

        # Make a fake entry for systemd-boot
        sudo mkdir -p $mnt/loader/entries/
        echo "default $3" | sudo tee $mnt/loader/loader.conf
        echo "title systemd-boot
version Test version
linux /bzImage
options quiet" | sudo tee $mnt/loader/entries/$3.conf

        sudo mkdir -p $mnt/boot/grub
        echo "source /EFI/boot/grub.cfg" | sudo tee $mnt/boot/grub/loopback.cfg

        sudo mkdir -p $mnt/grub
        echo "menuentry '$3 (Grub cfg)' {
    linux /bzImage quiet \${isoboot}
}" | sudo tee $mnt/grub/foo.cfg

        sudo mkdir -p $mnt/EFI/boot
        sudo cp fakeusb/nixos-grub.cfg $mnt/EFI/boot/grub.cfg
    fi
    sudo umount $mnt
    sudo losetup -d $part
}

rm -f $iso
truncate -s 20MiB $iso

echo "
mklabel gpt
mkpart primary 2048s 4095s
mkpart primary 4096s 100%
" | parted $iso

format_part "partiton #1 of " 2048
format_part "partiton #2 of " 4096 "fakeboot"
